<?php

class MapPlacesInstaller {

    private $wpdb;
    private $charset_collate;
    private $categories_table;
    private $markers_table;
    private $places_table;

    public function __construct() {
        global $wpdb;

        $this->wpdb = $wpdb;

        $this->categories_table = MP_TABLE_CATEGORIES;
        $this->markers_table    = MP_TABLE_MARKERS;
        $this->places_table     = MP_TABLE_PLACES;
        $this->charset_collate  = $this->wpdb->get_charset_collate();
    }

    /**
     * Method for initialize installer
     */
    public static function init() {
        register_activation_hook(MP_PLUGIN_DIR . 'map-places.php', [self::class, 'plugin_activation']);
        register_deactivation_hook(MP_PLUGIN_DIR . 'map-places.php', [self::class, 'plugin_deactivation']);
    }

    public static function plugin_activation () {
        // Create schema
        $instance = new self();
        $instance->create_tables();
        $instance->init_data();
    }

    public static function plugin_deactivation () {
        // Create schema
        $instance = new self();
        $instance->remove_tables();
    }

    /**
     * Create all schemas for the plugin
     */
    function create_tables () {
        // Create tables
        $this->create_categories_table();
        $this->create_markers_table();
        $this->create_places_table();
    }

    /**
     * Insert default data
     */
    public function init_data () {
        $init_data = [
            ['user_id' => get_current_user_id(), 'row_order' => 10, 'name' => 'Позитивні місця'],
            ['user_id' => get_current_user_id(), 'row_order' => 20, 'name' => 'Комерція'],
            ['user_id' => get_current_user_id(), 'row_order' => 30, 'name' => 'Проблемні місця'],
            ['user_id' => get_current_user_id(), 'row_order' => 40, 'name' => 'Неоднозначні місця'],
        ];

        // Insert categories
        foreach ($init_data as $category_data) { $this->wpdb->insert($this->categories_table, $category_data); }
    }

    /**
     * Create categories table
     */
    public function create_categories_table () {

        $sql = "CREATE TABLE $this->categories_table (
                    id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                    user_id bigint(20) NOT NULL,
                    row_order bigint(20),
                    name varchar(100) NOT NULL,
                    PRIMARY KEY id (id)
                ) $this->charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

    /**
     * Create categories table
     */
    public function create_markers_table () {

        $sql = "CREATE TABLE $this->markers_table (
                    id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                    user_id bigint(20) NOT NULL,
                    category_id bigint(20) unsigned NOT NULL,
                    approved boolean DEFAULT false NOT NULL,
                    name varchar(100) NOT NULL,
                    description varchar(500),
                    url varchar(500) DEFAULT '' NOT NULL,
                    PRIMARY KEY id (id),
                    KEY category_id (category_id)
                ) $this->charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

    /**
     * Create categories table
     */
    public function create_places_table () {

        $sql = "CREATE TABLE $this->places_table (
                    id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                    user_id bigint(20) NOT NULL,
                    marker_id bigint(20) unsigned NOT NULL,
                    approved boolean DEFAULT false NOT NULL,
                    name varchar(100),
                    description text,
                    PRIMARY KEY id (id),
                    KEY marker_id (marker_id)
                ) $this->charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

    public function remove_tables () {
        $this->wpdb->query("DROP TABLE $this->places_table;");
        $this->wpdb->query("DROP TABLE $this->markers_table;");
        $this->wpdb->query("DROP TABLE $this->categories_table;");
    }
}