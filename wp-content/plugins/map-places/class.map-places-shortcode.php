<?php

/**
 * Class MapPlacesShortCode
 * @author Ivan Kovbas kovbas.ivan@gmail.com
 */
class MapPlacesShortCode {

    /**
     * Method for initialize shortcode on client side
     */
    public static function init() {
        add_action('wp_print_styles', [self::class, 'register_styles'] ); // include styles
        add_action('wp_print_scripts', [self::class, 'register_scripts'] ); // include scripts

        // Add actions for ajax calls
        add_action( 'wp_ajax_create_marker', [MapPlaces::class, 'createMarker'] );
        add_action( 'wp_ajax_nopriv_create_marker', [MapPlaces::class, 'createMarker'] );

        add_action( 'wp_ajax_remove_marker', [MapPlaces::class, 'removeMarker'] );
        add_action( 'wp_ajax_nopriv_remove_marker', [MapPlaces::class, 'removeMarker'] );

        add_action( 'wp_ajax_get_init_data', [MapPlaces::class, 'getInitData'] );
        add_action( 'wp_ajax_nopriv_get_init_data', [MapPlaces::class, 'getInitData'] );

        // register shortcode
        add_shortcode('map-places-widget', [self::class, 'shortcode']);
    }

    public static function shortcode($atts, $content = null) {
        include(MP_VIEWS_DIR . 'widget.php');
    }

    /**
     * Include styles
     */
    public static function register_styles() {

        wp_register_style('font-awesome.min.css', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
        wp_enqueue_style('font-awesome.min.css');

        wp_register_style('jquery-ui.css', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('jquery-ui.css');

        wp_register_style('jquery-ui.css', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('jquery-ui.css');

        wp_register_style('leaflet.css', 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css');
        wp_enqueue_style('leaflet.css');

        wp_register_style('map-places.css', MP_CSS_URL . 'map-places.css');
        wp_enqueue_style('map-places.css');
    }

    /**
     * Include scripts
     */
    public static function register_scripts() {

        // Define ajax_object.ajax_url
        wp_enqueue_script( 'ajax-script', MP_JS_URL . 'ajax_object.js', ['jquery'] );
        wp_localize_script( 'ajax-script', 'ajax_url', admin_url( 'admin-ajax.php' ));

        $scripts = [

            // Vendors
            MP_JS_URL . 'vendor/jquery-ui.min.js',
            MP_JS_URL . 'vendor/angular.min.js',
            MP_JS_URL . 'vendor/angular-resource.min.js',
            MP_JS_URL . 'vendor/angular-route.min.js',
            MP_JS_URL . 'vendor/leaflet.js',

            // Components
//            MP_JS_URL . 'components/' . 'MpApplication.js',
//            MP_JS_URL . 'components/' . 'Category.js',
//            MP_JS_URL . 'components/' . 'Marker.js',
//            MP_JS_URL . 'components/' . 'MpMap.js',
//            MP_JS_URL . 'components/' . 'MpControlPanel.js',
            MP_JS_URL . 'components/' . 'CreateNewMarkerDialog.js',
            MP_JS_URL . 'components/' . 'ConfirmDialog.js',
            MP_JS_URL . 'components/' . 'MpProgressBar.js',

            MP_JS_URL . 'repositories.js',  // Services
            MP_JS_URL . 'directives.js',    // Directives
            MP_JS_URL . 'controllers.js',   // Controllers
            MP_JS_URL . 'bootstrap.js',     // Include bootstrap script
        ];

        // Include scripts
        foreach ($scripts as $script) {
            $scriptName = end(explode('/', $script));
            wp_register_script($scriptName, $script);
            wp_enqueue_script($scriptName);
        }
    }
}