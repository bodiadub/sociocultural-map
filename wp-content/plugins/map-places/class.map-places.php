<?php

/**
 * Class MapPlaces
 * @author Ivan Kovbas kovbas.ivan@gmail.com
 */
class MapPlaces {

    CONST ERROR_ACCESS_FORBIDDEN    = 'У вас немає прав для виконання цієї дії!';
    CONST ERROR_UNAUTHORIZED        = 'Для виконання цієї дії потрібно <a href="%s" title="Login">увійти</a> в систему!';

    CONST MARKER_NAME_MAX_LENGTH        = 100;
    CONST MARKER_DESCRIPTION_MAX_LENGTH = 500;
    const MAX_MARKER_SIZE               = 100;

    const REQUIRED_ERROR                = 'Обов\'язкове для заповнення.';
    const LENGTH_ERROR                  = 'Максимальна довжина %d символів.';
    const IMAGE_TYPE_ERROR              = 'Формат файлу повинен бути одним із наступних: %s.';
    const NO_CATEGORY_RECEIVED_ERROR    = 'Схоже щось пішло не так. Ми не можемо визначити категорію. Звернітсья до адміністратора.';
    const NO_CATEGORY_EXIST_ERROR       = 'Схоже що категорію, для якої ви намагаєтеся здійснити дію, було видалено. Перезавантажте сторінку і спробуйте знову.';

    const UNKNOWN_ERROR                 = 'Щось пішло не так, спробуйте пізніще, або звернітся до адміністратора.';

    const ALLOWED_MARKER_TYPES = [IMAGETYPE_PNG => 'png', IMAGETYPE_JPEG => 'jpeg'];

    /**
     * Method for creating new marker
     */
    public static function createMarker () {
        global $wpdb;

        // Check permissions
        self::exitIfNotLoggedIn();
        self::exitIfCantAddMarker();

        // VALIDATION
        $errors = [];

        // Validate name field
        if (empty($_POST['name'])) {
            $errors['name'][] = self::REQUIRED_ERROR;
        } elseif (!self::isLengthOk(trim($_POST['name']), 1, self::MARKER_NAME_MAX_LENGTH)) {
            $errors['name'][] = sprintf(self::LENGTH_ERROR, self::MARKER_NAME_MAX_LENGTH);
        }

        // Validate description field
        if (empty($_POST['description'])) {
            $errors['description'][] = self::REQUIRED_ERROR;
        } elseif (!self::isLengthOk(trim($_POST['name']), 1, self::MARKER_DESCRIPTION_MAX_LENGTH)) {
            $errors['description'][] = sprintf(self::LENGTH_ERROR, self::MARKER_DESCRIPTION_MAX_LENGTH);
        }

        // Validate Icon File
        if (empty($_FILES['marker_image'])) {
            $errors['marker_image'][] = self::REQUIRED_ERROR;
        } elseif (!array_key_exists(getimagesize($_FILES['marker_image']['tmp_name'])[2], self::ALLOWED_MARKER_TYPES)) {
            $errors['marker_image'][] = sprintf(self::IMAGE_TYPE_ERROR, implode(', ', self::ALLOWED_MARKER_TYPES));
        }

        // Return errors to the client if exist
        if ($errors) { self::sendResponse(400, ['errors' => $errors]); }

        // Validate category id
        if(empty($_POST['category_id'])) {self::sendResponse(400, ['message' => self::NO_CATEGORY_RECEIVED_ERROR]);}

        // Check if category exist
        $category_count = $wpdb->get_row("SELECT count('id') AS count FROM " . MP_TABLE_CATEGORIES . " WHERE id=" . intval($_POST['category_id']));
        if ($category_count->count != 1) {self::sendResponse(400, ['message' => self::NO_CATEGORY_EXIST_ERROR]);}

        // END OF VALIDATION
        // SAVE NEW MARKER

        $result = $wpdb->insert(MP_TABLE_MARKERS, [
            'user_id'       => strval(get_current_user_id()),
            'category_id'   => strval(intval($_POST['category_id'])),
            'approved'      => self::canApprove() ? '1' : '0',
            'name'          => trim($_POST['name']),
            'description'   => trim($_POST['description']),
            'url'           => self::uploadMarker($_FILES['marker_image']),
        ], ['%d', '%d', '%d', '%s', '%s', '%s']);
        if ($result == 0) {self::sendResponse(500, ['message' => self::UNKNOWN_ERROR]);}

        // Return created marker
        $marker = $wpdb->get_row("SELECT * FROM " . MP_TABLE_MARKERS . " WHERE id={$wpdb->insert_id}");
        $marker->url = MP_MARKERS_URL . $marker->url;
        self::sendOkResponse(['marker' => $marker]);
    }

    public static function removeMarker () {
        global $wpdb;

        // Check permissions
        self::exitIfNotLoggedIn();
        self::exitIfCantRemoveMarker(intval($_POST['marker_id']));

        $wpdb->delete(MP_TABLE_MARKERS, ['id' => trim($_POST['marker_id'])]);
        self::sendOkResponse(['message' => 'Маркер було успішно видалено']);
    }

    public static function getInitData () {
        global $wpdb;

        $result = $wpdb->get_results("
            SELECT
            c.id AS c_id, c.name AS c_name,
            m.id AS m_id, m.category_id AS m_category_id, m.approved AS m_approved,
            m.name AS m_name, m.description AS m_description, m.url AS m_url,
            p.id AS p_id, p.marker_id AS p_marker_id, p.approved AS p_approved,
            p.name AS p_name, p.description AS p_description
            FROM " . MP_TABLE_CATEGORIES . " c
            LEFT JOIN " . MP_TABLE_MARKERS . " m ON m.category_id = c.id
            LEFT JOIN " . MP_TABLE_PLACES . " p ON p.marker_id = m.id
            ORDER BY c.id, m.id, p.id
            "
        );

        $categories = [];

        $mapCategory = function ($data) {return ['id'=> $data->c_id, 'name' => $data->c_name, 'markers' => []];};
        $mapMarker = function ($data) {return ['id' => $data->m_id, 'category_id' => $data->m_category_id,
            'approved' => $data->m_approved, 'name' => $data->m_name, 'description'   => $data->m_description,
            'url' => MP_MARKERS_URL . $data->m_url, 'places' => []];};
        $mapPlace = function ($data) {return ['id' => $data->p_id, 'marker_id' => $data->p_marker_id,
            'approved' => $data->p_approved, 'name' => $data->p_name, 'description' => $data->p_description];};

        if (count($result) == 0) {self::sendOkResponse(['categories' => []]);}

        $categories[] = $mapCategory($result[0]);

        foreach ($result as $data) {
            if ($data->c_id != end($categories)['id']) {
                $categories[] = $mapCategory($data);
            }
            $category = &$categories[count($categories) - 1];

            if ($data->m_id == null) {continue;}
            if (empty($category['markers']) || $data->m_id != end($category['markers'])['id']) {
                $category['markers'][] = $mapMarker($data);
            }
            $marker = &$category['markers'][count($category['markers']) - 1];

            if ($data->p_id == null) {continue;}
            if (empty($marker['places']) || $data->p_id != end($marker['places'])['id']) {
                $marker['places'][] = $mapPlace($data);
            }
        }

        self::sendOkResponse(['categories' => $categories]);
    }

    /**
     * Upload image to marker directory and resize it if need
     *
     * @param $file
     * @return string
     */
    private static function uploadMarker ($file) {
        $image = new SimpleImage();
        $image->load($file['tmp_name']);

        // Get image width and height
        list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);

        // resize image if need
        if ($width >= $height && $width > self::MAX_MARKER_SIZE) {
            $image->resizeToWidth(self::MAX_MARKER_SIZE);
        } elseif ($height > self::MAX_MARKER_SIZE) {
            $image->resizeToHeight(self::MAX_MARKER_SIZE);
        }

        // Generate unique file name
        $fileName = md5_file($file['tmp_name']) . md5(microtime()) . '.' . end(explode(".", $file['name']));
        $image->save(MP_MARKERS_DIR . $fileName); // save image to marker directory

        // Return image file name
        return $fileName;
    }

    /** Methods for send response for the user */

    /**
     * Send UNAUTHORIZED response if client is not UNAUTHORIZED
     */
    static function exitIfNotLoggedIn () {
        if (!is_user_logged_in()) {
            self::sendResponse(401, ['message' => sprintf(self::ERROR_UNAUTHORIZED, self::generateLoginLink())]);
        }
    }

    /**
     * Send ACCESS_FORBIDDEN response if client don't have access to create new markers
     */
    static function exitIfCantAddMarker () {
        if (!self::canAddMarker()) {
            self::sendResponse(403, ['message' => self::ERROR_ACCESS_FORBIDDEN]);
        }
    }

    /**
     * Send ACCESS_FORBIDDEN response if client don't have access to remove the marker
     * @param $marker_id
     */
    static function exitIfCantRemoveMarker ($marker_id) {
        if (!self::canRemoveMarker($marker_id)) {
            self::sendResponse(403, ['message' => self::ERROR_ACCESS_FORBIDDEN]);
        }
    }

    /**
     * Send json response to the client with given status code and content
     *
     * @param $statusCode
     * @param $content
     */
    static function sendResponse ($statusCode, $content) {
        header( "Content-Type: application/json" );
        http_response_code($statusCode);
        wp_die(json_encode($content));
    }

    /**
     * Send json response to the client with http status code 200 and given content
     *
     * @param $content
     */
    static function sendOkResponse ($content = null) {
        self::sendResponse(200, $content);
    }

    /** Check privileges section */

    /**
     * Check if the current user can add marker
     *
     * @return bool
     */
    static function canAddMarker () {
        return is_user_logged_in();
    }

    /**
     * Check if the current user can remove marker
     * @param $marker_id
     * @return bool
     */
    static function canRemoveMarker ($marker_id) {
        global $wpdb;

        if (self::canModerate()) {return true;}

        // Get marker with given id
        $marker_user_id = $wpdb->get_results("SELECT user_id FROM " . MP_TABLE_MARKERS . " " . "WHERE id=" . intval($marker_id));
        if (!isset($marker_user_id[0])) {return true;}

        return get_current_user_id() == $marker_user_id[0]->user_id;
    }

    /**
     * Check if the current user can add category
     *
     * @return bool
     */
    static function canAddCategory () {
        return self::canModerate();
    }

    /**
     * Check if the current user can approve added elements
     *
     * @return bool
     */
    static function canApprove () {
        return self::canModerate();
    }

    /**
     * Check if the current user can moderate content
     *
     * @return bool
     */
    static function canModerate () {
        return current_user_can('administrator') || current_user_can('editor');
    }

    /** Help functions */

    /**
     * @return string
     */
    private static function generateLoginLink () {
        return wp_login_url( '/' );
    }

    /**
     * @param $string
     * @param $min
     * @param $max
     * @return bool
     */
    private static function isLengthOk ($string, $min, $max) {
        return (strlen($string) >= $min) && (strlen($string) <= $max);
    }
}