var mapPlaces = angular.module('mapPlaces', [
    'ngRoute', 'mapPlacesControllers', 'mapPlacesRepositories', 'mapPlacesDirectives'
]);

document.mpProgressBar = new MpProgressBar();

mapPlaces.constant('API_PATH', ajax_object.ajax_url);

//jQuery(function() {
//
//    // Initialize progress bar
//    document.mpProgressBar = new MpProgressBar();
//    document.mpProgressBar.show();
//
//    jQuery.post(ajax_object.ajax_url, {action : 'get_init_data', data : []})
//        .done(function (response) {
//            (new MpApplication(document.getElementById('map-places-wrapper'))).init(response);
//        }).fail(function(response) {
//            //showErrors(response);
//            console.log(response);
//        }).always(function () {
//            document.mpProgressBar.hide();
//        });
//});
