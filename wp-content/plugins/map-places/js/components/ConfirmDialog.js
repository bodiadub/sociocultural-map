var ConfirmDialog = function () {

    var message     = null;
    var onConfirm   = function () {};

    var dialog = jQuery( "#confirm-dialog" ).dialog({
        autoOpen: false,
        resizable: false,
        width: 350,
        closeOnEscape: false,
        draggable: false,
        modal: true,
        buttons: {
            "first": {
                text: 'Відмінити',
                class: 'btn btn-default',
                click: function() { dialog.dialog( "close" ); }
            },
            "second": {
                text: 'Так',
                class: 'btn btn-primary',
                click: onConfirm
            }
        },
        open: function(event, ui) {
            jQuery(event.target).dialog('widget')
                .css({ position: 'fixed' })
                .position({ my: 'center', at: 'center', of: window });

            jQuery(window).resize(function() {
                jQuery(event.target).dialog('widget')
                    .position({ my: 'center', at: 'center', of: window });
            });
        },
        close: function() {
        }
    });

    return {
        open : function (tmpMessage, callback) {
            message     = tmpMessage;
            onConfirm   = callback;
            dialog.dialog( "open" );
        },
        close : function () {
            dialog.dialog( "close" );
        }
    };
};
