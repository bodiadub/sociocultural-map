/**
 * Created by ikovbas on 17.04.16.
 */
var mapPlacesControllers = angular.module('mapPlacesControllers', ['ngResource']);

mapPlacesControllers.controller('MapCtrl', ['$scope', 'Repository',
    function ($scope, Repository) {

        $scope.categories = [];
        var mapUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
        var startX = 48.92278;
        var startY = 24.71035;
        var startZ = 13;

        var map = L.map('map').setView([startX, startY], startZ);
        L.tileLayer(mapUrl).addTo(map);

        // Initialize dialogues
        var createNewMarkerDialog   = new CreateNewMarkerDialog();
        var confirmDialog           = new ConfirmDialog();

        Repository.getInitData().then(function (data) {
            $scope.categories = data.categories;
            console.log('ok', data);
        }, function (error) {
            console.log(error);
        });

        $scope.initAccordion = function (selector) {
            jQuery(selector).accordion({
                header: "> div > h3",
                active: null,
                animate: 200,
                collapsible: true,
                heightStyle: 'content'
            });
        };

        $scope.addMarkerByData = function (marker_data) {
            for (var i=0; i < $scope.categories.length; i++) {
                if ($scope.categories[i].id == marker_data.category_id) {
                    $scope.categories[i].markers.push(marker_data);
                }
            }
            $scope.$apply();
        };

        /**
         * Remove marker form categories markers
         *
         * @param marker
         */
        $scope.removeMarker = function (marker) {
            $scope.categories.forEach(function (category) {
                if (category.id == marker.category_id) {
                    category.markers.splice(category.markers.indexOf(marker), 1);
                }
            });
        };

        $scope.openCreateMarkerDialog = function (category) {
            createNewMarkerDialog.open(category, function (markerData) {
                $scope.addMarkerByData(markerData);
                createNewMarkerDialog.close();
            });
        };

        $scope.deleteMarker = function (marker) {
            confirmDialog.open("Ви дійсно хочете видалити маркер '" + marker.name + "'?" , function () {
                $scope.removeMarker(marker);
                createNewMarkerDialog.close();
            });
        }
    }
]);