<div id="progress-wrapper">
    <div class="progress-background"></div>
    <table><tr><td><div class="progress-message">Loading...</div></td></tr></table>
</div>
<div class="map-places-wrapper" ng-app="mapPlaces">
    <?php include(MP_VIEWS_DIR . 'create-marker-dialog.php'); ?>
    <?php include(MP_VIEWS_DIR . 'confirm-dialog.php'); ?>
    <div ng-controller="MapCtrl">
        <div class="map-places-wrapper">
            <div class="mp-map">
                <div class="map" id="map"></div>
            </div>
            <div class="mp-nav">
                <div class="control-groups">
                    <input class="left-button" type="button" value="сховати всі">
                    <input class="right-button" type="button" value="додати">
                    <div id="accordion" class="ui-accordion ui-widget ui-helper-reset" role="tablist">
                        <div ng-repeat="category in categories" repeat-complete="initAccordion('#accordion')">
                            <h3 class="ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-id-4" aria-controls="ui-id-5" aria-selected="false" aria-expanded="false" tabindex="-1">
                                <span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
                                {{category.name}}
                            </h3>
                            <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-id-5" aria-labelledby="ui-id-4" role="tabpanel" aria-hidden="true" style="display: none;">
                                <ul>
                                    <li ng-repeat="marker in category.markers">
                                        <div class="marker-wrapper" ng-init="opened=false" ng-class="{'marker-control-opened':opened, 'marker-control-closed':!opened}">
                                            <table class="marker-controls">
                                                <tr>
                                                    <td class="marker-control-wrapper control-clickable edit-button">
                                                        <i class="marker-control fa fa-pencil-square-o"></i>
                                                    </td>
                                                    <td class="marker-control-wrapper control-clickable remove-button" ng-click="deleteMarker(marker)">
                                                        <i class="fa fa-times"></i>
                                                    </td>
                                                    <td class="marker-control-wrapper control-clickable" ng-click="opened=!opened">
                                                        <div class="open-button">
                                                            <i class="fa" ng-class="{'fa-chevron-right':!opened, 'fa-chevron-left':opened}"></i>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr class="control-clickable">
                                                    <td class="image-wrapper marker-control-wrapper">
                                                        <img ng-src="{{marker.url}}" />
                                                    </td>
                                                    <td class="text-wrapper marker-control-wrapper">
                                                        <div class="title-wrapper" title="{{marker.name}}">{{marker.name}}</div>
                                                        <div class="description-wrapper" title="Test">{{marker.description}}</div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </li>
                                </ul>
                                <a href="" class="add-marker-button" ng-click="openCreateMarkerDialog(category)">Додати Новий Маркер</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div id="map-places-wrapper"  class="map-places-wrapper">-->
<!--</div>-->